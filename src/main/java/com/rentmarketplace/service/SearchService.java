package com.rentmarketplace.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrResponse;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.model.search.SearchConstants;
import com.rentmarketplace.model.search.SearchRequest;
import com.rentmarketplace.repository.ProductRepository;

@Transactional
@Service
public class SearchService {
	
	public static final Logger logger = LogManager.getLogger(SearchService.class);
	
	@Autowired
	private ProductService productService;

	@Autowired
	private ProductRepository productRepository;

    private static SolrClient solrClient;

	public List<Product> getSearchResultProducts(SearchRequest searchRequest) {
		logger.debug("getSearchResult() started");
		
		String searchString = searchRequest.getSearchString();

		// get List of products
		List<Product> products = productService.findAll();
		
		// set up solr
		solrClient = new HttpSolrClient(SearchConstants.SOLR_URL);
		logger.debug("solrClient is gotten.");

		//		List<SolrInputDocument> documents = new ArrayList<>();
		UpdateResponse updateResponse = null;
		try {
			updateResponse = solrClient.addBeans(products);
		} catch (SolrServerException | IOException e1) {
			logger.error("ERROR! Exception while executing: updateResponse = solrClient.addBeans(products);");
			logger.error("Cannot connect to Solr. Run Solr server to fix it and restart this application.");
			return null;
//			e1.printStackTrace();
		}
		
		// commit solr
		try {
			solrClient.commit();
		} catch (SolrServerException | IOException e) {
			logger.error("ERROR! Exception while executing: solr.commit();");
			e.printStackTrace();
		}
		logger.debug("solrClient commited.");

		
		// create parameters of query for search
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.add("q", searchString);
		logger.debug("query is added to solrClient.");
		
		// get response from solr
		QueryResponse queryResponse = null;
		try {
			queryResponse = solrClient.query(solrQuery);
		} catch (SolrServerException e) {
			logger.error("ERROR! Exception while executing: QueryResponse response = solrClient.query(parameters);");
			e.printStackTrace();
		}
		logger.debug("queryResponse is gotten.");
		
		// get result of search
		SolrDocumentList solrDocumentList = queryResponse.getResults();
		logger.debug("result is gotten from queryResponce.");
		
		// get strings from result
		List<String> searchResultList = new ArrayList<>();
		List<Product> searchResultProductsList = new ArrayList<>();
		for (SolrDocument solrDocument : solrDocumentList) {
			searchResultList.add(solrDocument.toString());
			System.out.println("--- " + solrDocument.toString());
			Product product = new Product();
			product.setId(Integer.valueOf((String)(solrDocument.getFieldValue("id"))));
			product.setName((String) solrDocument.getFieldValue("name"));
			product.setDescription((String) solrDocument.getFieldValue("description"));
			searchResultProductsList.add(product);
		}
		
		return searchResultProductsList;
	}

/*	
	private static String testSearch(String string) {
		// create a SolrInputDocumet with data
		String urlString = "http://localhost:8983/solr/techproducts";
		SolrClient solrClient = new HttpSolrClient(urlString);
		SolrInputDocument document1 = new SolrInputDocument();
		document1.addField("id", "552199");
		document1.addField("name", "Gouda cheese wheel");
		document1.addField("price", "49.99");
		SolrInputDocument document2 = new SolrInputDocument();
		document2.addField("id", "11111");
		document2.addField("name", "Gouda 22222");
		document2.addField("price", "3333.3333");
		UpdateResponse updateResponse = null;
		try {
			updateResponse = solrClient.add(document1);
			updateResponse = solrClient.add(document2);
		} catch (SolrServerException | IOException e) {
			logger.error("Exception during: UpdateResponse response = solr.add(document);");
			e.printStackTrace();
		}
		 
		// Remember to commit your changes!
		try {
			solrClient.commit();
		} catch (SolrServerException | IOException e) {
			logger.error("Exception during: solr.commit();");
			e.printStackTrace();
		}
		
		// create parameters
		SolrQuery parameters = new SolrQuery();
		parameters.set("q", string);
		
		
		// submit parameters with query
		QueryResponse queryResponse = null;
		try {
			queryResponse = solrClient.query(parameters);
		} catch (SolrServerException e) {
			logger.error("Exception during: QueryResponse response = solr.query(parameters);");
			e.printStackTrace();
		}
		
		SolrDocumentList list = queryResponse.getResults();
		
		for (SolrDocument result : list) {
			System.out.println("list of results: " + result.toString());
		}
		
		
		return list.get(0).toString();
		
	}	
*/
	
}
