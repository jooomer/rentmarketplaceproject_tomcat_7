package com.rentmarketplace.service;

import java.io.File;
import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.sql.rowset.serial.SerialBlob;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.rentmarketplace.model.entity.Image;

@Service
public class FileService {
	
	private static final Logger logger = LogManager.getLogger(FileService.class);
			
	@Autowired
	private ServletContext servletContext;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	public File cacheMultipartFile(MultipartFile multipartFile) {
		logger.debug("--- start");
		String classPathFile = "resources/cache/" + multipartFile.getOriginalFilename();
		File file = null;
		try {
			file = resourceLoader.getResource(classPathFile).getFile();
			multipartFile.transferTo(file);
		} catch (Exception e) {
			logger.error("ERROR! Can not write image to file.");
			e.printStackTrace();
		} 
		return file;
	}

	public File cacheImage(Image image) {
		logger.debug("--- start");
		String classPathFile = "resources/cache/" + image.getFileName();
		File file = null;
		try {
			file = resourceLoader.getResource(classPathFile).getFile();
			FileUtils.writeByteArrayToFile(file, image.getBlobImage().getBytes(1, (int) image.getBlobImage().length()));
		} catch (Exception e) {
			logger.error("ERROR! Can not write image to a harddisk.");
			e.printStackTrace();
		} 
		
		long imageSize = 0;
		try {
			imageSize = image.getBlobImage().length();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
//		// just check out if the file has already written
//		if (!isImageCached(file, imageSize)) {
//			logger.error("File has not written.");
//		}
//		
//		try {
//			Thread.sleep(5000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}

		return file;
	}

//	private boolean isImageCached(File cachedFile, long imageSize) {
//		long cachedFileSize = FileUtils.sizeOf(cachedFile);
//		System.out.println("imageSize:      " + imageSize);
//		System.out.println("cachedFileSize: " + cachedFileSize);
//		return true;
//	}

	public Image createImage(String classPathFile) {
		logger.debug("--- start");
		Image image = new Image();
		File file = null;
		try {
			file = resourceLoader.getResource(classPathFile).getFile();
			Blob blobImage = new SerialBlob(FileUtils.readFileToByteArray(file));
			image.setBlobImage(blobImage);
		} catch (Exception e) {
			logger.debug("Error occurred while getting Blob from a file");
			e.printStackTrace();
		}
		image.setFileName(file.getName());
		return image;
	}

	public void cleanFolder(String classPathFolder) {
		logger.debug("--- start");
		File folder = null;
		try {
			folder = resourceLoader.getResource(classPathFolder).getFile();
			if (folder.exists()) {
				folder.delete();				
			}
			folder.mkdir();
		} catch (IOException e) {
			logger.debug("Error occurred while getting directory recource: " + folder);
			e.printStackTrace();
		}
	}

	
	
}
