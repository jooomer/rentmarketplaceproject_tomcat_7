package com.rentmarketplace.service;

import java.io.IOException;
import java.sql.Blob;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.sql.rowset.serial.SerialBlob;
import javax.transaction.Transactional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.method.P;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.rentmarketplace.controller.registered.AddProductController;
import com.rentmarketplace.model.dto.AddProductDto;
import com.rentmarketplace.model.entity.Image;
import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.model.entity.ProductType;
import com.rentmarketplace.model.entity.User;
import com.rentmarketplace.repository.ProductRepository;

@Service
@Transactional
public class ProductService {

	private static final Logger logger = LogManager.getLogger(ProductService.class);

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private ProductTypeService productTypeService;
	
	@Autowired
	private FileService fileService;

	public List<Product> findAll() {
		return productRepository.findAll();
	}

	public Product findOne(int id) {
		return productRepository.findOne(id);
	}

	public List<Product> findAllByUserName(String name) {
		List<Product> myProducts = productRepository.findAllByUserName(name);
		return myProducts;
	}

	public void save(Product product, String name) {
		User user = userService.findOneWithProducts(name);
		product.setUser(user);
		String productTypeName = product.getProductType().getName();
		ProductType productType = productTypeService.findByName(productTypeName);
		product.setProductType(productType);
		productRepository.save(product);
	}

	public Product findOneByName(String name) {
		return productRepository.findOneByName(name);
	}

	@PreAuthorize(value = "#product.user.name == authentication.name or hasRole('ROLE_ADMIN')")
	public void delete(@P("product") Product product) {
		productRepository.delete(product);
	}

	public List<Product> findAllByProductType(ProductType productType) {
		return productRepository.findByProductType(productType);
	}

	public void save(Product product) {
		productRepository.save(product);
		
	}

	public int save(AddProductDto productDto, MultipartFile multipartFile, String userName) {

		logger.debug("--- start");

		// create new Product and get data from DTO
		Product product = new Product();
		productDto.getAllFields(product);
		
		// create and set Image
		Image image = new Image();
		try {
			Blob blobImage = new SerialBlob(multipartFile.getBytes());
			image.setBlobImage(blobImage);
		} catch (SQLException | IOException e) {
			logger.debug("Error occurred while getting Blob from MultipartFile");
			e.printStackTrace();
		}
		image.setFileName(multipartFile.getOriginalFilename());
		product.setImage(image);
		image.setProduct(product);
		
		// set current date 
		product.setPublishedDate(new Date());

		// set product type with Id
		ProductType productType = product.getProductType();
		productType = productTypeService.findByName(productType.getName());
		product.setProductType(productType);
		
		// set name of user to new product
		User user = userService.findOneWithProducts(userName);
		product.setUser(user);

		// save new product in DB
		productRepository.save(product);
		
		// save image to file on disc
		fileService.cacheMultipartFile(multipartFile);

		return product.getId();
	}


}
