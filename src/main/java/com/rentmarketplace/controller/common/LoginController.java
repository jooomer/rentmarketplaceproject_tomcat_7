package com.rentmarketplace.controller.common;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/login")
public class LoginController {

	// handle request "/login"
	// call login.jsp to show login form
	@RequestMapping
	public String login() {
		return "login";
	}
	
}
