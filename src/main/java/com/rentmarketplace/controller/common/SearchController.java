package com.rentmarketplace.controller.common;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.model.search.SearchRequest;
import com.rentmarketplace.service.SearchService;

@Controller
public class SearchController {

	private static final Logger logger = LogManager.getLogger(SearchController.class);
	
	@Autowired
	private SearchService searchService;
	
	@ModelAttribute("searchRequest")
	public SearchRequest construct() {
		return new SearchRequest();
	}
	
	@RequestMapping("/search")
	public String showSearch() {
		
		logger.debug("showSearch() started");

		return "search";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String doSearch(@ModelAttribute("searchRequest") SearchRequest searchRequest, Model model) {
	
		logger.debug("doSearch() started");

		String string = ((SearchRequest) searchRequest).getSearchString();
		System.out.println("from SearchRequest: " + string);
		
		List<Product> searchResultProductsList = searchService.getSearchResultProducts(searchRequest);
		
		if (searchResultProductsList == null) {
			model.addAttribute("message", "Search does not work.");
		}

		model.addAttribute("searchString", string);
		model.addAttribute("searchResultProductsList", searchResultProductsList);
		return "search";

	}
}
