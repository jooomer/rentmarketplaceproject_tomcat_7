package com.rentmarketplace.controller.common;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rentmarketplace.model.entity.User;
import com.rentmarketplace.service.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {
	
	@Autowired
	private UserService userService;
	
	// prepare entity User for registration form
	@ModelAttribute("user")
	public User construct() {
		return new User();
	}
	
	// handle request "/register"
	// call register.jsp to show register form
	@RequestMapping
	public String showRegister() {
		return "register";
	}
	
	// handle request "/register" with data from register form
	// Entity User consists all data from register form
	// 
	@RequestMapping(method = RequestMethod.POST)
	public String doRegister(@Valid @ModelAttribute("user") User user, BindingResult result,
			RedirectAttributes redirectAttributes) {
		
		// check validation of data from register form
		// if not valid - call register form again
		if (result.hasErrors()) {
			return "register";
		}
		
		// save user in DB
		userService.save(user);
		
		// set attribute "success" to show success message 
		redirectAttributes.addFlashAttribute("success", true);
		
		// call register.jsp
		return "redirect:/register";
	}

}
