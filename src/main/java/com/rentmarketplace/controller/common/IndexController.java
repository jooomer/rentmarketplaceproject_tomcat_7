package com.rentmarketplace.controller.common;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes("user")
@RequestMapping("/")
public class IndexController {
	
	// handle request "/"
	// call index.jsp to show home page
	@RequestMapping
	public String viewMainPage() {
		return "index";
	}
	
	
}
