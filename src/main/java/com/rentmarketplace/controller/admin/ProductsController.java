/*
 * 
 */

package com.rentmarketplace.controller.admin;

import java.io.File;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.rentmarketplace.controller.registered.AddProductController;
import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.service.FileService;
import com.rentmarketplace.service.ProductService;

@Controller
public class ProductsController {
	
	private static final Logger logger = LogManager.getLogger(AddProductController.class);

	@Autowired
	private ProductService productService;
	
	@Autowired
	private FileService fileService;
		
	/**
	 * just shows list of products
	 */
	@RequestMapping("/products")
	public String showProducts(Model model) {
		logger.debug("--- start");
		List<Product> products = productService.findAll();
		model.addAttribute("products", products);
		return "products";
		
	}
	
	/**
	 * just shows product detail
	 */
	@RequestMapping("/products/{id}")
	public String showProductDetail(Model model, @PathVariable int id) {
		logger.debug("--- start");
		
		// get one product from DB by id
		Product product = productService.findOne(id	);
		
		// get image from DB and prepare it to display
		if (product.getImage() != null) {
			logger.debug("Product doesn't contain image.");
			File imageFile = fileService.cacheImage(product.getImage());
//			logger.debug("Cache image path: " + "/resources/cache/" + imageFile.getName());
//			logger.debug("Full image path: " + imageFile);
			model.addAttribute("image", "/resources/cache/" + imageFile.getName());
		}
		
		// prepare attributes
		model.addAttribute("product", product);
		
		// call product-detail.jsp
		return "product-detail";
	}
	
}
