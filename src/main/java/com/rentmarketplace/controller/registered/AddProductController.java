package com.rentmarketplace.controller.registered;

import java.security.Principal;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

//import jdk.nashorn.internal.objects.annotations.Constructor;









import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rentmarketplace.model.dto.AddProductDto;
import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.model.entity.User;
import com.rentmarketplace.repository.ProductRepository;
import com.rentmarketplace.service.FileService;
import com.rentmarketplace.service.ProductService;
import com.rentmarketplace.service.ProductTypeService;
import com.rentmarketplace.service.UserService;

@Controller
public class AddProductController {

	private static final Logger logger = LogManager.getLogger(AddProductController.class);

	@Autowired
	private ProductService productService;

	@Autowired
	private ProductTypeService productTypeService;

	@Autowired
	private UserService userService;
	
	/**
	 * prepare entity Product for add-product form
	 */
	@ModelAttribute("product")
	public AddProductDto construct() {
		return new AddProductDto();
	}

	/**
	 * show add-product form
	 */
	@RequestMapping(value = "/add-product")
	public String showAddProduct(Model model) {
		logger.debug("--- method started");
		model.addAttribute("listOfProductTypes", productTypeService.findAll());
		return "add-product";
	}

	/**
	 * receives new Product and saved it into DB
	 */
	@RequestMapping(value = "/add-product", method = RequestMethod.POST)
	public String doAddProduct(
			@Valid @ModelAttribute("product") AddProductDto productDto,
			BindingResult result, 
			Principal principal, 
			Model model,
			@RequestParam(value = "image", required = false) MultipartFile multipartFile,
			RedirectAttributes redirectAttributes) {

		logger.debug("--- method started");
		
		// check data validation from form
		// if not valid - show add-product form again
		if (result.hasErrors()) {
			logger.debug("Validation form data error.");
			return showAddProduct(model);
		}

		// validate image
		if (!isImageValid(multipartFile)) {
			logger.debug("Image is not valid.");
			return "redirect:/add-product";
		}
		
		// save new product
		int id = productService.save(productDto, multipartFile, principal.getName());
		
		// prepare image class path to display 
//		model.addAttribute("image", "/resources/cache/" + multipartFile.getOriginalFilename());
		redirectAttributes.addFlashAttribute("image", 
				"/resources/cache/" + multipartFile.getOriginalFilename());
		
		// show product detail by id
		// set parameter "success" to show message in a product detail page
		return "redirect:/my-products/" + id + "?success=true";
	}

	/**
	 * validates image
	 * returns image type or null if not valid
	 */
	private boolean isImageValid(MultipartFile image) {
		switch(image.getContentType()) {
		case "image/jpeg": 
			return true;
		case "image/png": 
			return true;
		default:
			return false;
		}
	}

}
