package com.rentmarketplace.controller.registered;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.Principal;
import java.sql.Blob;
import java.sql.SQLException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.model.entity.User;
import com.rentmarketplace.service.FileService;
import com.rentmarketplace.service.ProductService;
import com.rentmarketplace.service.UserService;

@Controller
@RequestMapping("/my-products")
public class MyProductsController {
	
	private static final Logger logger = LogManager.getLogger(MyProductsController.class);

	@Autowired
	private ProductService productService;
	
	@Autowired
	private FileService fileService;
		
	/**
	 * displays all products of current user
	 */
	@RequestMapping
	public String showMyProducts(Model model, Principal principal) {
		logger.debug("--- start");
		String name = principal.getName();
		model.addAttribute("myProducts", productService.findAllByUserName(name));
		return "my-products";
		
	}
	
	/**
	 * displays particular product by its Id
	 */
	@RequestMapping("/{strId}")
	public String detail(Model model, @PathVariable String strId,
			final HttpServletResponse response) {
		logger.debug("--- start");
		
		// get clear id of product
		int id = Integer.valueOf(strId.replace("?success=true", ""));
		
		// get one product from DB by id
		Product product = productService.findOne(id	);
		
		if (product.getImage().getBlobImage() == null) {
			System.out.println("------------------------- No bLobImage");
		}
		
		File imageFile = fileService.cacheImage(product.getImage());
		logger.debug("Cache image path: " + "/resources/cache/" + imageFile.getName());
		model.addAttribute("image", "/resources/cache/" + imageFile.getName());
		
		// prepare attributes
		model.addAttribute("product", product);
		model.addAttribute("success", true);
		
		// call product-detail.jsp
		return "product-detail";
	}

}
