package com.rentmarketplace.model.entity;

import java.sql.Blob;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;


@Entity
public class Image {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
//	@Lob
//	@Column(columnDefinition = "mediumblob")
//	private byte[] image;

	@Column(name = "blob_image")
	private Blob blobImage;

	@Column(name = "file_name")
	private String fileName;
	
	@OneToOne //(mappedBy = "image")
	@JoinColumn(name = "product_id")
	private Product product;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Blob getBlobImage() {
		return blobImage;
	}

	public void setBlobImage(Blob blobImage) {
		this.blobImage = blobImage;
	}
	
}
