package com.rentmarketplace.model.dto;

import javax.validation.constraints.Size;

import com.rentmarketplace.model.entity.Product;
import com.rentmarketplace.model.entity.ProductType;

public class AddProductDto {

	@Size(min = 3, message = "The product name must be at least 3 characters!")
	private String name;
	
	private String description;

	private Double price;
	
	private ProductType productType;

	public Product getAllFields(Product product) {
		product.setName(name);
		product.setDescription(description);
		product.setPrice(price);
		product.setProductType(productType);
		return product;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public ProductType getProductType() {
		return productType;
	}

	public void setProductType(ProductType productType) {
		this.productType = productType;
	}

	public void getAllFields() {
		// TODO Auto-generated method stub
		
	}
	

}
