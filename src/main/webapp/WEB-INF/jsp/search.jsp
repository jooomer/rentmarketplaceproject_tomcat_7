<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ include file="/WEB-INF/layout/taglib.jsp" %>

<form:form commandName="searchRequest" cssClass="form-horizontal">

	
		<div class="form-group">
			<div class="col-sm-10">
				<form:input path="searchString" cssClass="form-control"/>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-10">
				<input type="submit" value="Search" class="btn btn-lg btn-primary" />
			</div>
		</div>

</form:form> 

<c:if test="${empty searchResultProductsList}" >
	<h3>${message}</h3>
</c:if>

<c:if test="${not empty searchResultProductsList}" >

<h3>Search result for request "<spring:message text="${searchString}" />"</h3>
<table class="table table-bordered table-hover table-stripped">
	<tbody>
		<c:forEach items="${searchResultProductsList}" var="product">
			<tr>
				<td>
					<a href='<spring:url value="/products/${product.id}" />'>${product.name}</a>
					<p><strong>Description:</strong><spring:message text=" ${product.description}" /></p>
				</td>
			</tr>
		</c:forEach>
	</tbody>

</table>

</c:if>

